# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################  
#   Code by: Enrique AvendaÃ±o Trujillo e-mail: trujillo2.2.1@gmail.com
##############################################################################

from openerp.osv import fields, osv, orm
from datetime import time, datetime
from openerp.tools.translate import _


class reporte_altas(osv.osv):

	_name = 'reporte.altas'
	_description = 'Reporte de alta de clientes'
	_columns = {
		'name': fields.char(required=True, readonly=True, states={'draft': [('readonly', False)]}, size=40, copy=False, help="Coloca la Referencia del reporte en este campo, pueden ser numeros o letras."),
		'date_order': fields.date('Fecha', required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}, copy=False, help="Fecha en la que se realiza el reporte."),
		'active': fields.boolean('Activo', required=True, readonly=True, states={'draft': [('readonly', False)]}, copy=False),
		'responsable_id': fields.many2one('res.partner', 'Responsable', readonly=True , states={'draft': [('readonly', False)]}, required=True, help="Selecciona el Usuario o responsable de la realización del reporte." ),
		'nota': fields.text('Notas', required=False, readonly=True, states={'draft': [('readonly', False)]}, copy=False),
		'verif_cutomer': fields.boolean('Reporte de clientes', readonly=True, states={'draft': [('readonly', False)]}, help="Selecciona este campo si tu reporte será de clientes."),
		'verif_supplier': fields.boolean('Reporte de proveedores', readonly=True, states={'draft': [('readonly', False)]}, help="Selecciona este campo si tu reporte será de proveedores."),
		'verif_user': fields.boolean('Reporte de usuarios', readonly=True, states={'draft': [('readonly', False)]}, help="Selecciona este campo si tu reporte será de usuarios."),
		'date_one': fields.date('Inicio', required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}, copy=False, help="Selecciona la fecha inicial del reporte."),
		'date_two': fields.date('Termino', required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}, copy=False, help="Selecciona la fecha final del reporte."),				
		'clientes_ids': fields.many2many('res.partner', 'report_cliente', 'partner_id', string='Clientes', priority=False, readonly=True, states={'draft': [('readonly', False)]}, copy=False, help="""Da click en 'Agregar un elemento' 
		selecciona y agrega los clientes que aparecen en la tabla, todos ellos son los que se agregaron al sistema entre las fechas seleccionadas."""),
		'proveedores_ids': fields.many2many('res.partner', 'report_proveedor', 'partner_id', string='Proveedores', priority=False, readonly=True, states={'draft': [('readonly', False)]}, copy=False, help="""Da click en 'Agregar un elemento' 
		selecciona y agrega los proveedores que aparecen en la tabla, todos ellos son los que se agregaron al sistema entre las fechas seleccionadas."""),
		'usuarios_ids': fields.many2many('res.partner', 'report_usuario', 'partner_id', string='Usuarios', priority=False, readonly=True, states={'draft': [('readonly', False)]}, copy=False, help="""Da click en 'Agregar un elemento' 
		selecciona y agrega los usuarios que aparecen en la tabla, todos ellos son los que se agregaron al sistema entre las fechas seleccionadas."""),
		'state': fields.selection([('draft','Borrador'),('confirmed','Confirmado'),('cancel','Cancelado')], 'Estado', readonly=True),
		}

	_defaults = {
		'date_order': fields.date.context_today,
		'state': 'draft',
		'active': True,
		}
#Ordena los campos
	_order = 'date_order desc, id desc'


	def confirm(self, cr, uid, ids, context = None):
		self.write(cr, uid, ids, {'state': 'confirmed'}, context = None)
		return True

	def cancel(self, cr, uid, ids, context = None):
		self.write(cr, uid, ids, {'state': 'cancel'}, context = None)
		return True

	def draft(self, cr, uid, ids, context = None):
		self.write(cr, uid, ids, {'state': 'draft'}, context = None)
		return True

reporte_altas()


class res_partner(osv.osv):
    _inherit = 'res.partner'
    _columns = {
		'discharge_date': fields.date('Fecha de alta', required=True),
	    }

    _defaults = {
        'discharge_date': fields.date.context_today,
    }

    _order = 'discharge_date desc, id desc'

res_partner()