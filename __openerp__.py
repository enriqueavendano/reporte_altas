# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################  
#   Code by: Enrique Avendaño Trujillo e-mail: trujillo2.2.1@gmail.com
##############################################################################

{
    'name': 'REPORTE DE ALTAS DE CLIENTES, PROVEEDORES Y USUARIOS',
    'version': '1.0',
    'author': 'Enrique Avendaño Trujillo',
    'category': 'Reporte',
    'description': """
        Es un módulo que facilita al usuario la creación de reportes por fecha de alta
        de clientes, proveedores y usuarios, pudiendo anexar a ellos notas u observaciones de los grupos
        reportados.
         """,
    'website': 'https://maxsoluciones.ddns.net',
    'license': 'AGPL-3',
    'depends': ['sale'],
    'init_xml': [],
    'demo_xml': [],
    'update_xml': [
                'reportealtas_view.xml',
                'reportealtas_report.xml',
                'views/reporte_altas.xml',
                 ],
    'installable': True,
    'active': False,
}
